import React from 'react';
import { BrowserRouter, Switch, Route,Link } from "react-router-dom";
import logo from './logo.svg';
import './assets/css/App.css';
import Header from './components/sections/Header.js';
import Footer from './components/sections/Footer.js';
import Perfil from './components/pages/Perfil.js';
import Experiencia from './components/pages/Experiencia.js';
import Contact from './components/pages/Contact.js';
import Proyecto from './components/pages/Proyecto.js';
import NotFound from './components/pages/NotFound.js';

function App() {
  return (
    <div className="App">
     <BrowserRouter>
      <Header />
        <Switch>
          <Route exact path="/" component={Perfil}>
            <Perfil />
          </Route>
          <Route exact path="/Experiencia" component={Experiencia}>
            <Experiencia />
          </Route>
          <Route exact path="/Contact" component={Contact}>
            <Contact />
          </Route>
          <Route exact path="/Proyecto" component={Proyecto}>
            <Proyecto />
          </Route>
          <Route component={NotFound}/>
        </Switch>
        <Footer />
     </BrowserRouter>
    </div>
  );
}

export default App;
