import React from 'react';

export default class Perfil extends React.Component{

	constructor(props){
		super(props);
	}

	render(){
		return (
			<div className="container standar">
				<div className="row">
					<div className="col-md-12 col-lg-3">
						<div className="contentImg">
							<img src={require('../../assets/img/andres.jpg')} className="img-fluid img-thumbnail" alt="Responsive image"/>
						</div>
						<div className="ListInfo">
							<ul>
								<li className="itemInfo">
                                    <h4>Nacionalidad:</h4>
                                    <p>Venezolano</p>
								</li>
								<li className="itemInfo">
                                    <h4>Nombre Completo:</h4>
                                    <p>Andres David Medina Moreno</p>
								</li>
								<li className="itemInfo">
                                    <h4>Edad:</h4>
                                    <p>28 Años</p>
								</li>
								<li className="itemInfo">
                                    <h4>Titulos:</h4>
                                    <p>Ingeniero Informatico</p>
                                    <p>Administrador de Empresas</p>
								</li>
							</ul>
						</div>
					</div>
					<div className="col-md-12 col-lg-9">
						<div className="contentGeneralPerfil">
							<h3>Biografía</h3>
							<p className="space">En mis inicios en la universidad estudie Administracion de Empresas me interesaba saber todo lo relevante a mercadotecnia y publicidad (Estudio del mercado, Analisis Foda, Ciclos del producto, etc...), esto me ayudo a determinar que el futuro estaba orientado a soluciones tecnologicas para empresas</p>
							<p className="space">Inicie mi formacion con un curso de mantenimiento y reparacion de PCs con SO windows (Trabaje como Soportista).</p> 
							<p className="space">En los años proximos postule a una beca para estudiar Ingenieria Informatica por medio de la cual obtuve mi titulo de Ingeniero Informatico con perfil de Analista, siendo esta una carrera prometedora con tantas areas en donde desempeñarse era necesario enfocarse en una. </p>
							<p className="space">Inspeccione algunas de las areas como : Soporte Tecnico, CCNA, Programacion Desktop, Lider de Requerimientos y Programacion Web.</p>
							<p className="space">Al analizar todas mis experiencias encontre mi bocacion en el desarrollo web en especifico el desarrollo Front End.</p>
							<p className="space">Actualmente tengo 7 años de experiencia en el desarrollo web Front End integrando Api Rest para generar interfaces dinamicas, trabajando de la mano de las areas comerciales para lograr cumplir con los requerimientos de los proyectos.</p>
							<h3>Conocimientos</h3>
							<p>Repositorio : Bitbucket, Gitlab.</p>
							<p>Manejo de Git.</p>
							<p>Lenguajes: Javascript, Typescript.</p>
							<p>Frameworks: Angular js, Laravel, Symfony, SCRUM, Angular 5-6-7-8, Ionic V1-V3.</p>
							<p>Librerias: Webpack, babel, jquery, Bootstrap, React js.</p>
							<p>Herramientas Back End: Node js + Express.</p>
							<p>Enmaquetado: HTML + CSS.</p>
							<p>Animaciones: CSS3 + Keyframes.</p>
						</div>
					</div>
				</div>
			</div>
		)
	}
}