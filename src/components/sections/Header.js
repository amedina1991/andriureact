import React,{Fragment} from 'react';
import { BrowserRouter, Switch, Route,Link } from "react-router-dom";

export default class Header extends React.Component{

	constructor(props){
		super(props);
	}

	render(){
		return (
      <div className="container">
  			<nav className="navbar navbar-expand-lg navbar-light bg-light">
    				<a className="navbar-brand" href="#">Front End Developer</a>
    				<button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      				<span className="navbar-toggler-icon"></span>
    				</button>
  				  <div className="collapse navbar-collapse  w-100 order-3 dual-collapse2" id="navbarSupportedContent">
      				<ul className="navbar-nav ml-auto">
              			<li className="nav-item">
                  			<Link  to="/" className="nav-link">Perfil Profesional</Link>
              			</li>
              			<li className="nav-item">
                  			<Link  to="/Experiencia" className="nav-link">Experiencia</Link>
              			</li>
              			<li className="nav-item">
                  			<Link to="/Proyecto" className="nav-link">Proyectos</Link>
              			</li>
              			<li className="nav-item">
                  			<Link  to="/Contact" className="nav-link">Contacto</Link>
              			</li>
      				 </ul>
    				</div>
  			</nav>
      </div>
		)
  }
}