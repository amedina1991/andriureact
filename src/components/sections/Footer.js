import React from 'react';

export default class Footer extends React.Component{

	constructor(props){
		super(props);
	}

	render(){
		return (
			<div className="container">
				<footer id="footer">
					<div className="contenInfo">
						<p>Todos los derechos reservados a mi.</p>
					</div>
				</footer>
			</div>
		)
	}
}